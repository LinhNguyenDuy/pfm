import React from 'react';
/* eslint-disable react/prefer-stateless-function */
import logo from '../../../assets/images/logo_phc.png';
class Header extends React.Component {
  render() {
    return (
      <div className="block-header-container">
        <div className="block-header-content">
          <a className="button-logo">
            <img className="logo button-logo" src={logo} alt="Logo" />
          </a>
        </div>
       </div> 
    );
  }
}

export default Header;
