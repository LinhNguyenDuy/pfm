import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { checkNotUndefined } from 'Containers/App/constantsFunc';

const showNoteError = (condition, conditionTrue) => (condition ? conditionTrue : '');

const toBytesUTF8 = (chars) => {
  return unescape(encodeURIComponent(chars));
};

const fromBytesUTF8 = (bytes) => {
  return decodeURIComponent(escape(bytes));
}

const limitByte = (element, maxByte) => {
  let bytes = toBytesUTF8(element.value);
  if (bytes.length > maxByte) {
    while (true) {
      try {
        if (fromBytesUTF8(bytes) && bytes.length <= maxByte) {
          break;
        }
      } catch (error) {
        
      }
      bytes = bytes.substring(0, bytes.length -1);
    }
  }
  element.value = fromBytesUTF8(bytes);
};

class ReduxInput extends PureComponent {
  state = {  }
  render() { 
    const {input, isRequired, label, maxLength, maxByte, type, disable, nonErrorText, readOnly, meta: {error, touched}, labelInline, isTestInput, hideArrow} = this.props;
    return ( 
      <div className ="field-form">
        <div className="group-input-form">
          {checkNotUndefined(label) ? <label className={`title-filed-form${checkNotUndefined(labelInline) ? ' label-inline-form' : ''}`}>{label}{checkNotUndefined(isRequired) ? <sup className="star-notify">※</sup> : null}</label> : null}
          <input
            {...input}
            onKeyPress={this.handleOnKeyPress}
            onChange={this.handleOnChange}
            id={`input-${input.name}`}
            type={checkNotUndefined(type) ? type : 'text'}
            maxLength={maxLength}
            onKeyDown={maxByte ? limitByte(input, maxByte) : () => { }}
            disabled={disabled}
            className={`${showNoteError(error && touched, 'note-error-input')}${checkNotUndefined(labelInline) ? ' text-inline-form' : ''}${checkNotUndefined(isTestInput) ? ' test-input' : ''}${checkNotUndefined(hideArrow) ? ' hide-arrow' : ''}`}
            readOnly={readOnly}
          />
          {checkNotUndefined(isTestInput) ? <div className="btn-test-input">{isTestInput.button(input.value, input.value === '' || checkNotUndefined(error), isRequired)}</div> : null}
        </div>
        {error && touched && !nonErrorText ? (<span className={`message-err${checkNotUndefined(labelInline) ? ' error-inline-form' : ''}`}>{error}</span>) : null}
      </div>
     );
  }
}

ReduxInput.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string,
  meta: PropTypes.object,
  isRequired: PropTypes.bool,
  nonErrorText: PropTypes.bool,
  maxLength: PropTypes.number,
  disable: PropTypes.bool,
  labelInline: PropTypes.bool,
  readOnly: PropTypes.bool,
  isTestInput: PropTypes.object,
  maxByte: PropTypes.number,
  hideArrow: PropTypes.bool,
};
 
export default ReduxInput;