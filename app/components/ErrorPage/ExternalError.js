import React, { PureComponent } from 'react';

class ExternalError extends PureComponent {
  constructor(props) {
    super(props);
    document.title = '404 - File or directory not found.';
  }

  componentWillUnmount() {
    document.title = 'PHC Website';
  }

  render() {
    return (
      <div className="external-error-page">
        <h1 className="title-error">Server Error</h1>
        <div className="content-error">
          <fieldset>
            <h5 className="title-code">404 - File or directory not found.</h5>
            <p className="message">The resource you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p>
          </fieldset>
        </div>
      </div>
    );
  }
}

export default ExternalError;
