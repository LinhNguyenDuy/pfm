import React from 'react';
import PropTypes from 'prop-types';

const ReduxField = ({input, label, type, id, isRequired, maxLength, nonErrorText, meta: { error, touched }}) => (
  <div className = "redux-field field-form">
    <label className="title-field-form">{label}<span style = {isRequired ? {color: 'red'} : {display: 'none'}}>※</span></label>
    <input {...input} id={id} maxLength={maxLength} className ={error && touched ? 'input-error' : ''} type={type} />
    <span className="error-message">{error && touched && !nonErrorText ? error : ''}</span>
  </div>
);

ReduxField.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string,
  id: PropTypes.string,
  meta: PropTypes.object,
  isRequired: PropTypes.bool,
  nonErrorText: PropTypes.bool,
  maxLength: PropTypes.number,
};

export default ReduxField;