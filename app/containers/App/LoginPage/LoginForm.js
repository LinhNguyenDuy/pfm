import React, { PureComponent } from 'react';
import {Field, reduxForm} from 'redux-form';
import PropTypes from 'prop-types';
import ReduxInput from 'Components/ReduxInput';
const validate= (values) => {
  const errors = {};
  return errors;
};

class LoginForm extends PureComponent {
  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props
    return (
      <form onSubmit={handleSubmit}>
      <Field
        name="contractNo"
        type="text"
        component={ReduxInput}
        label="X_ID"
        
      />
      <Field
        name="loginId"
        type="text"
        component={ReduxInput}
        label="LoginID"
        
      />
      <Field
        name="password"
        type="password"
        component={ReduxInput}
        label="Password"
      />
      <div>
        <button type="submit" disabled={submitting}>
          Forget
        </button>
        <button type="button" disabled={pristine || submitting} onClick={reset}>
          Login
        </button>
      </div>
    </form>
  )
}
}
export default reduxForm({
  form: 'loginForm',
  validate,
})(LoginForm);