import React from 'react';

export const LOAD_REPOS = 'boilerplate/App/LOAD_REPOS';
export const LOAD_REPOS_SUCCESS = 'boilerplate/App/LOAD_REPOS_SUCCESS';
export const LOAD_REPOS_ERROR = 'boilerplate/App/LOAD_REPOS_ERROR';
export const DEFAULT_LOCALE = 'en';
export const SET_PREVIOUS_PATH = 'SET_PREVIOUS_PATH';

export const pagination = (prefix) => `${prefix}_PAGINATION`;
export const paginationSuccess = (prefix) => `${prefix}_PAGINATION_SUCCESS`;
export const paginationFailure = (prefix) => `${prefix}_PAGINATION_FAILURE`;

export const totalRecordInTable = 50;
export const currentPage = 0;
export const firstPage = 0;

export const ICON = {
  DISABLE_ALARM: <img key="alarm-disable" src="/assets/images/icon-img/alarm-disable.png" alt="disable-alarm" />,
  COMMUNICATION_ABNORMAL: <img key="communication-abnormal" src="/assets/images/icon-img/communication-abnormal.png" alt="communication-abnormal" />,
  SETTING_ABNORMAL: <img key="setting-abnormal" src="/assets/images/icon-img/setting-abnormal.png" alt="setting-abnormal" />,
  SUBDEVICE_ABNORMAL: <img key="subdevice-abnormal" src="/assets/images/icon-img/subdevice-abnormal.png" alt="subdevice-abnormal" />,
  SENSOR_ABNORMAL: <img key="sensor-abnormal" src="/assets/images/icon-img/sensor-abnormal.png" alt="sensor-abnormal" />,
  PIN_1_LEVEL: <img key="pin-1-level" src="/assets/images/icon-img/pin-1-level.png" alt="pin-1-level" />,
  PIN_2_LEVEL: <img key="pin-2-level" src="/assets/images/icon-img/pin-2-level.png" alt="pin-2-level" />,
  FLASH_1_LEVEL: <img key="flash-1-level" src="/assets/images/icon-img/flash-1-level.png" alt="flash-1-level" />,
  FLASH_2_LEVEL: <img key="flash-2-level" src="/assets/images/icon-img/flash-2-level.png" alt="flash-2-level" />,
  ADAPTER_ABNORMAL: <img key="adapter-abnormal" src="/assets/images/icon-img/adapter-abnormal.png" alt="adapter-abnormal" />,
  CONTACT_INPUT: <img key="contact-input" src="/assets/images/icon-img/contact-input.png" alt="contact-input" />,
  OVER_WARNING: <img key="over-warning" src="/assets/images/icon-img/over-warning.png" alt="over-warning" />,
  OVER_ERROR: <img key="over-error" src="/assets/images/icon-img/over-error.png" alt="over-error" />,
};

export const imgIcon = '/assets/images/icon-img/';
export const imgIconStatus = '/assets/images/icon-img/status-device/';
export const fileSize = 52428800;
export const maxByte = 256;

export const ERROR_TIMEOUT = '設定した時間だけWebサーバーとの通信がないためアプリからログアウトします。';
export const ERROR_NETWORK = 'サーバーにデータを送信したが、指定時間が過ぎてもレスポンスが返ってこないため、処理(通信)を中断します。';
export const ERROR_NODATA = 'データがありません。';

export const USE_FULL_SIZE_MESSAGE = 'ドキュメント共有の使用量を超えたためレポートを保存できませんでした';
export const CONTRACT_EXPIRED_MESSAGE = '下記お客様の本サービスご利用契約が満了しました。ご確認願います。';

export const COM_18 = 'セッションタイムアウト'; // session timeout
export const COM_15 = '処理中です 。しばらくお待ちください。'; // message loading
export const MESSAGE_INPUT_EMPTY = '※未入力です'; // message loading

export const Message = {
  COM_11: '※未入力です。',
};

export const ABNORMAL_CASE_1 = -10000;
export const ABNORMAL_CASE_2 = -2147483648;
