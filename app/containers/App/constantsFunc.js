import { SubmissionError, change } from 'redux-form';
import { push } from 'react-router-redux';
import moment from 'moment';
import * as APP_CONST from 'Containers/App/constants';

const formatDateTime = {
  date: 'YYYY/MM/DD',
  time: 'HH:mm',
};

export const checkNotUndefinedAndNull = (value) => value !== undefined && value !== null;

export const formatTimezone = (value) => (value && value !== null ? moment(value).format(`${formatDateTime.date} ${formatDateTime.time}`) : '');

export const formatDateTimeNotSecond = (value) => (moment(value).format(`${formatDateTime.date} ${formatDateTime.time}`));

export const formatTimezoneDate = (value) => value ? moment(value).format(formatDateTime.date) : '';

export const formatTimezoneTime = (value) => value ? moment(value).format(formatDateTime.time) : '';

export const formatDateTimeAlarmMobile = (value, facility) => value ? `${moment(value).format('HH:mm')} ${facility} on ${moment(value).format('YYYY/MM/DD')}` : '';

export const formatDateForExport = (value) => value ? `${moment(value).format('YYYYMMDD')}` : '';

export const formatLong2Date = (value, format) => (value ? moment(value).format(format) : '');

export const formatString2Date = (value, valueFormat, format) => (value ? moment(value, valueFormat).format(format) : '');

export const convertDatetoLong = (value) => value ? Date.parse(value) : 0;

export const checkNotUndefined = (value) => (typeof value !== 'undefined');

export const checkMessageNotify = (notify, success, fail) => notify ? success : fail;

export const getToken = () => {
  return `bearer ${localStorage.getItem('PanaToken')}`;
};

export const convertObjByField = (arr, labelField, valueField) => {
  const tempArr = [];
  if (checkNotUndefined(arr) && arr !== null) {
    for (let i = 0; i < arr.length; i += 1) {
      const element = arr[i];
      tempArr.push({ label: element[labelField], value: element[valueField] });
    }
  }
  return tempArr;
};

export const convertStringToObjectDD = (arrStr) => {
  if (checkNotUndefinedAndNull(arrStr)) {
    const tempArr = [];
    for (let i = 0; i < arrStr.length; i += 1) {
      const element = arrStr[i];
      tempArr.push({ label: element, value: element });
    } return tempArr;
  }
  return [];
};

export const convertFileList = (list, checkType) => {
  const items = [];
  if (list.length > 0 && checkType(list)) {
    for (let i = 0; i < list.length; i += 1) {
      const element = list[i];
      items.push({ name: element.name, size: `${(element.size / 1024)} KB` });
    }
  }
  return items;
};

export const changeDataDD = (value, field1) => {
  const tempArr = [];
  if (checkNotUndefined(value) && checkNotUndefined(value.length) && value.length > 0) {
    for (let i = 0; i < value.length; i += 1) {
      const element = value[i];
      tempArr.push({
        label: element[field1],
        value: element,
      });
    }
  }
  return tempArr;
};

export const checkCloseClick = (isCreateSuccessfully, func) => isCreateSuccessfully ? func : () => { };

export const checkEnoughField = (objValue, arrField) => {
  for (let i = 0; i < arrField.length; i += 1) {
    const element = arrField[i];
    if (!checkNotUndefined(objValue[element]) || objValue[element] === null || objValue[element] === '') {
      return true;
    }
  }
  return false;
};

export const removeFieldInObject = (obj, arrField) => {
  const body = Object.assign(obj);
  for (let i = 0; i < arrField.length; i += 1) {
    delete body[arrField[i]];
  }
};

export const removeObjFromArr = (value, field, arr) => arr.filter((el) => el[field] !== value);

export const getValueDD = (arr, arrVariable) => {
  let tempObj = {};
  for (let i = 0; i < arrVariable.length; i += 1) {
    const element = arrVariable[i];
    tempObj = { ...tempObj, [element]: arr[element].value };
  }
  return tempObj;
};

const getArrVariable = (obj) => {
  if (checkNotUndefined(obj) && Object.keys(obj).length > 0) {
    const tempArr = [];
    for (const key in obj) {
      tempArr.push(key);
    }
    return tempArr;
  }
  return [];
};

export const setDefaultValue = (formName, objSet, dispatch) => {
  const arrVariable = getArrVariable(objSet);
  for (let i = 0; i < arrVariable.length; i += 1) {
    dispatch(change(formName, `${arrVariable[i]}`, objSet[arrVariable[i]]));
  }
};

export const setValueDateTimePicker = (formName, field, value) => (dispatch) => {
  dispatch(change(formName, field, value));
};

export const redirectPath = (path) => (dispatch) => {
  dispatch(push(path));
};

export const checkBtn = (value, type) => (dispatch) => {
  dispatch({ type, data: value });
};

export const sortDec = (arrayData, field, order, typeSearch) => {
  if (typeSearch === 'boolean' || typeSearch === 'number') {
    bubbleSort(arrayData, field, order);
    return arrayData;
  }
  return arrayData.sort(compareValues(field, order, typeSearch));
};

export const sortInc = (arrayData, field, order, typeSearch) => {
  if (typeSearch === 'boolean' || typeSearch === 'number') {
    bubbleSort(arrayData, field, order);
    return arrayData;
  }
  return arrayData.sort(compareValues(field, order, typeSearch));
};

export const notifyMessage = (obj) => {
  throw new SubmissionError(obj);
};

function convertToCSV(objArray) {
  const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
  let str = '';

  for (let i = 0; i < array.length; i += 1) {
    let line = '';
    Object.keys(array[i]).forEach((index) => {
      if (line !== '') line += ',';

      line += array[i][index];
    });

    str += `${line}\r\n`;
  }
  return str;
}

export const exportCSVFile = (headers, items, fileTitle, user, fileName) => {
  if (headers) {
    items.unshift(headers);
    items.unshift({ blank: '' });
  }

  items.unshift({ date: `Output Date Time,${formatLong2Date(new Date(), 'YYYY/MM/DD HH:mm:ss')}` });
  if (user) {
    items.unshift({ user: `UserID,${user}` });
  }
  if (fileTitle) {
    items.unshift({ fileTitle });
  }

  // Convert Object to JSON
  const jsonObject = JSON.stringify(items);
  const csv = `\ufeff${convertToCSV(jsonObject)}`;
  const exportedFilenmae = fileName ? `${fileName}.csv` : 'export.csv';

  const blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });

  if (navigator.msSaveBlob) { // IE 10+
    navigator.msSaveBlob(blob, exportedFilenmae);
  } else {
    const link = document.createElement('a');
    if (link.download !== undefined) { // feature detection
      // Browsers that support HTML5 download attribute
      const url = URL.createObjectURL(blob);
      link.setAttribute('href', url);
      link.setAttribute('download', exportedFilenmae);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
};

const compareValues = (key, order = 'ASC', typeSearch) => function (a, b) {
  let keyDeviceList;
  let aDeviceList;
  let bDeviceList;
  if (key === 'measure1') {
    aDeviceList = a.meauringData[0] || {};
    bDeviceList = b.meauringData[0] || {};
    keyDeviceList = 'measurement';
  } else if (key === 'value1') {
    aDeviceList = a.meauringData[0] || {};
    bDeviceList = b.meauringData[0] || {};
    keyDeviceList = 'value';
  } else if (key === 'measure2') {
    aDeviceList = a.meauringData[1] || {};
    bDeviceList = b.meauringData[1] || {};
    keyDeviceList = 'measurement';
  } else if (key === 'value2') {
    aDeviceList = a.meauringData[1] || {};
    bDeviceList = b.meauringData[1] || {};
    keyDeviceList = 'value';
  }
  if ((key !== 'measure1' && key !== 'value1' && key !== 'measure2' && key !== 'value2') && (!a.hasOwnProperty(key) || !b.hasOwnProperty(key))) {
    return 0;
  }
  var varA = '';
  var varB = '';
  if (typeSearch === 'text') {
    if (key === 'measure1' || key === 'value1' || key === 'measure2' || key === 'value2') {
      if (!aDeviceList.hasOwnProperty(keyDeviceList) || aDeviceList[keyDeviceList] === null) {
        aDeviceList[keyDeviceList] = '';
      } else if (aDeviceList[keyDeviceList] === APP_CONST.ABNORMAL_CASE_1 || aDeviceList[keyDeviceList] === APP_CONST.ABNORMAL_CASE_2) {
        aDeviceList[keyDeviceList] = '―';
      }
      if (!bDeviceList.hasOwnProperty(keyDeviceList) || bDeviceList[keyDeviceList] === null) {
        bDeviceList[keyDeviceList] = '';
      } else if (bDeviceList[keyDeviceList] === APP_CONST.ABNORMAL_CASE_1 || bDeviceList[keyDeviceList] === APP_CONST.ABNORMAL_CASE_2) {
        bDeviceList[keyDeviceList] = '―';
      }
      const com = compateString(aDeviceList[keyDeviceList], bDeviceList[keyDeviceList]);
      return ((order === 'DESC') ? (com * -1) : com);
    } else {
      if (a[key] == null) {
        a[key] = '';
      } else if (b[key] == null) {
        b[key] = '';
      }
      var com = compateString(a[key], b[key]);
      return ((order === 'DESC') ? (com * -1) : com);
    }
  } else if (typeSearch === 'date') {
    varA = new Date(a[key]);
    varB = new Date(b[key]);
  } else {
    varA = a[key];
    varB = b[key];
  }

  let comparison = 0;
  if (varA > varB) {
    comparison = 1;
  } else if (varA < varB) {
    comparison = -1;
  }
  return (
    (order == 'DESC') ? (comparison * -1) : comparison
  );
};

const compateString = (a, b) => (a >= b) - (a <= b);

// const compateString = (a, b) => {
//   let reParts = /\d+|\D+/g;
//   let reDigit = /\d/;
//   let aParts = a.match(reParts);
//   let bParts = b.match(reParts);
//   // Used to determine if aPart and bPart are digits.
//   let isDigitPart;
//   // If `a` and `b` are strings with substring parts that match...
//   if (aParts && bParts &&
//     (isDigitPart = reDigit.test(aParts[0])) == reDigit.test(bParts[0])) {
//     // Loop through each substring part to compare the overall strings.
//     let len = Math.min(aParts.length, bParts.length);
//     for (let i = 0; i < len; i++) {
//       let aPart = aParts[i];
//       let bPart = bParts[i];
//       // If comparing digits, convert them to numbers (assuming base 10).
//       if (isDigitPart) {
//         aPart = parseInt(aPart, 10);
//         bPart = parseInt(bPart, 10);
//       }
//       // If the substrings aren't equal, return either -1 or 1.
//       if (aPart != bPart) {
//         return aPart < bPart ? -1 : 1;
//       }
//       // Toggle the value of isDigitPart since the parts will alternate.
//       isDigitPart = !isDigitPart;
//     }
//   }
//   return (a >= b) - (a <= b);
// };

export const checkItemsResponse = (value) => {
  if (value !== null) {
    if (typeof value !== 'undefined' && typeof value.items !== 'undefined' && value.items.length > 0) {
      return true;
    }
  }
  return false;
};

export const convertParamSearch = (obj) => {
  if (!checkNotUndefinedAndNull(obj)) return '';
  const arrVariable = Object.keys(obj);
  let params = '';
  for (let i = 0; i < arrVariable.length; i += 1) {
    const element = arrVariable[i];
    if (checkNotUndefined(obj[element]) && obj[element] !== '' && typeof obj[element] !== 'object') {
      params += `&${element}=${obj[element]}`;
    }
  }

  return params;
};

export const convertDataDropdownToShow = (array) => {
  if (checkNotUndefinedAndNull(array)) {
    const arrTemp = [];
    for (let i = 0; i < array.length; i += 1) {
      const element = array[i];
      const obj = {
        label: element.value,
        value: element.key,
      };
      arrTemp.push(obj);
    }
    return arrTemp;
  }
  return [];
};

export const convertParamSearchWithSortProperty = (obj) => {
  const params = convertParamSearch(obj);
  const paramsConverted = params.replace('headerColumn', 'sortProperty');
  return paramsConverted;
};

export const convertParamSearchWithSortPropertyIncludeDate = (obj) => {
  const params = convertParamSearchIncludeDate(obj);
  const paramsConverted = params.replace('headerColumn', 'sortProperty');
  return paramsConverted;
};

export const removeElementArrSelectOption = (value, arr) => {
  const temp = arr;
  const temp2 = temp.filter((el) => el !== value);
  return temp2;
};

export const convertParamSearchIncludeDate = (obj) => {
  if (!checkNotUndefinedAndNull(obj)) return '';
  const arrVariable = Object.keys(obj);
  let params = '';
  for (let i = 0; i < arrVariable.length; i += 1) {
    const element = arrVariable[i];
    if (checkNotUndefined(obj[element]) && obj[element] !== '') {
      if (checkNotUndefined(obj[element].from) && obj[element].from !== null) {
        params += `&${element}Start=${obj[element].from}`;
        if (checkNotUndefined(obj[element].to) && obj[element].to !== null) {
          params += `&${element}End=${obj[element].to}`;
        }
      } else {
        params += `&${element}=${obj[element]}`;
      }
    }
  }
  return params;
};

export const initializeResponse = () => ({
  currentPage: 0,
  isFirstPage: true,
  isLastPage: true,
  items: [],
  numberOfElements: 0,
  pageSize: 0,
  totalElements: 0,
  totalPages: 0,
});

export const checkResponseForTable = (list) => !Array.isArray(list) && checkNotUndefined(list) && list !== null ? list : initializeResponse();

export const checkDefaulHeadertSort = (arrTitle) => {
  let obj = [];
  arrTitle.map((value, index) => {
    let objHeader = arrTitle[index];
    if (objHeader.defaultSort) {
      obj.push(objHeader);
    }
  });
  return obj;
};

function bubbleSort(a, par, type) {
  let swapped;
  do {
    swapped = false;
    for (let i = 0; i < a.length - 1; i++) {
      if (type == 'ASC') {
        if (a[i][par] > a[i + 1][par]) {
          var temp = a[i];
          a[i] = a[i + 1];
          a[i + 1] = temp;
          swapped = true;
        }
      } else if (a[i][par] < a[i + 1][par]) {
        var temp = a[i];
        a[i] = a[i + 1];
        a[i + 1] = temp;
        swapped = true;
      }
    }
  } while (swapped);
}

/**
 * Validate password follow the security policy
 * @param   {string} password Password need validate.
 * @returns {boolean} - true - password containing at least 8 characters, 1 number, 1 upper, 1 lowercase and 1 specific character.
 *                    - false - otherwise.
 */

export const validatePassword = (password) =>
  // return /^(?=.*?\d)(?=.*?[A-Z])(?=.*?[a-z])(?=.*[!#$%&@])[a-zA-Z\d!#$%&@]{8,}$/.test(password);
  /(^(?=.*?\d)(?=.*?[A-Za-z])[a-zA-Z\d!#$%&]{8,16}$)|(^(?=.*?[A-Za-z])(?=.*[!#$%&])[a-zA-Z\d!#$%&]{8,16}$)|(^(?=.*?\d)(?=.*[!#$%&])[a-zA-Z\d!#$%&]{8,16}$)/.test(password);

const parseIntID = (id) => parseInt(id, 10);

export const checkFavoriteGraph = (favoriteGraphId, listFavoriteGraph) =>
  checkNotUndefined(listFavoriteGraph) && checkNotUndefined(favoriteGraphId) ?
    listFavoriteGraph.find((e) => e.id === parseIntID(favoriteGraphId))
    : undefined;

export const checkProperty = (propertyId, listDevice) =>
  checkNotUndefined(listDevice) && checkNotUndefined(propertyId) ?
    listDevice.find((e) => e.contractId === parseIntID(propertyId))
    : undefined;

export const checkFacility = (facilityId, listDevice) =>
  checkNotUndefined(listDevice) && checkNotUndefined(facilityId) ?
    listDevice.find((e) => e.facilityId === parseIntID(facilityId))
    : undefined;

let temp = undefined;
let title = '';
const checkMapHierarchyByLevel = (mapHierarchyId, list) => {
  for (let i = 0; i < list.length; i += 1) {
    const element = list[i];
    title = `${title && title !== '' ? `${title} > ` : ''}${element ? element.mapHierarchyName : ''}`;
    if (element.mapHierarchyId === Number(mapHierarchyId)) {
      temp = element;
      temp.title = title;
    } else {
      if (Array.isArray(element.mapHierarchies) && element.mapHierarchies.length === 0) {
        title = '';
      }
      checkMapHierarchyByLevel(mapHierarchyId, element.mapHierarchies);
    }
  }
  return temp;
};

export const checkMapHierachy = (mapHierarchyId, facility) => {
  title = '';
  return checkNotUndefined(facility) && checkNotUndefined(mapHierarchyId) ? checkMapHierarchyByLevel(mapHierarchyId, facility.mapHierarchies) : undefined;
};

export const checkMapId = (mapId, mapHierachy) =>
  checkNotUndefined(mapHierachy) && checkNotUndefined(mapId) ?
    mapHierachy.maps.find((e) => e.mapId === parseIntID(mapId))
    : undefined;

export const checkDeviceId = (deviceId, map) =>
  checkNotUndefined(map) && checkNotUndefined(deviceId) ?
    map.devices.find((e) => e.deviceId === parseIntID(deviceId))
    : undefined;

export const checkMeasureId = (measureId, device) =>
  checkNotUndefined(device) && checkNotUndefined(measureId) ?
    device.measures.find((e) => e.measureId === parseIntID(measureId))
    : undefined;

export const validateRequireField = (values, arrElement) => {
  const errors = {};
  arrElement.forEach((element) => {
    if (Array.isArray(values[element])) {
      if (values[element].length === 0) {
        errors[element] = '※未入力です。';
      }
    } else if (!values[element] || values[element] === null) {
      if (element !== 'subnetMask' && element !== 'defaultGateway') {
        errors[element] = '※未入力です。';
      }
    } else {
      if ((element === 'subdeviceIpAddress' || element === 'subnetMask' || element === 'defaultGateway') && !/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/.test(values[element])) {
        errors[element] = '※入力に誤りがあります。';
      }
    }
  });
  return errors;
};

export const getMessageByCode = (messageArray, code) => {
  messageArray.find((message) => message.code === code);
};

export const getMessageById = (messageArray, id) => {
  messageArray.find((message) => message.id === id);
};

export const validatePasswordField = (values, arrElement, errorsRequire) => {
  const errors = errorsRequire;
  arrElement.forEach((element) => {
    if (!checkNotUndefined(errors[element]) || errors[element] === null) {
      if (!validatePassword(values[element])) {
        errors[element] = '新規パスワードの入力形式に間違いがあります。再度入力してください。';
      } else if (values.newPass !== values.confirmPass) {
        errors.confirmPass = 'パスワードが確認用パスワードと一致していません。再度入力してください。';
      }
    }
  });
  return errors;
};

export const validateSpaceInId = (value, previousValue) => {
  if (/\s/.test(value)) {
    return checkNotUndefined(previousValue) ? previousValue.replace(/\s/g, '') : previousValue;
  }
  return checkNotUndefined(value) ? value.replace(/\s/g, '') : value;
};

export const getByteFromString = (value) => Buffer.byteLength(value, 'utf-8');

export const controlResponseCode = (objectError, code) => {
  const obj = objectError.find((e) => e.code === code);
  return checkNotUndefined(obj) ? obj : null;
};

export const resetStoreInReducer = (type) => (dispatch) => {
  dispatch({ type });
};

const toBytesUTF8 = (chars) => unescape(encodeURIComponent(chars));

const fromBytesUTF8 = (bytes) => decodeURIComponent(escape(bytes));

export const limitByteNotForForm = (element, maxByte) => {
  let bytes = toBytesUTF8(element.val());
  if (bytes.length > maxByte) {
    while (true) {
      try {
        if (fromBytesUTF8(bytes) && bytes.length <= maxByte) {
          break;
        }
      } catch (error) { }
      bytes = bytes.substring(0, bytes.length - 1);
    }
  }
  element.prop('value', fromBytesUTF8(bytes));
};

export const detectMobileDevice = () => navigator.userAgent.match(/Android/i)
  || navigator.userAgent.match(/webOS/i)
  || navigator.userAgent.match(/iPhone/i)
  || navigator.userAgent.match(/iPod/i)
  || navigator.userAgent.match(/BlackBerry/i)
  || navigator.userAgent.match(/Windows Phone/i);
