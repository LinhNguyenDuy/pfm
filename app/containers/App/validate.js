const required = (value, isArr) => value && (isArr ? value.length > 0 : true) ? undefined : '※未入力です。';

const maxLength = (max) => (value) => value && value.length > max ? `Must be ${max} characters or less` : undefined;

const minLength = (min) => (value) => value && value.length < min ? `Must be ${min} characters or more` : undefined;

const number = (value) => value && isNaN(Number(value)) ? 'Must be a number' : undefined;

const minValue = (min) => (value) => value && value < min ? `Must be at least ${min}` : undefined;

// Invalid email address
const email = (value) => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'メールアドレスが正しくありません。' : undefined;

const tooOld = (value) => value && value > 65 ? 'You might be too old for this' : undefined;

const aol = (value) => value && /.+@aol\.com/.test(value) ? 'Really? You still use AOL for your email?' : undefined;

// Only alphanumeric characters
const alphaNumeric = (value) => value && !(!/[~`!#$%\^&*+@=\-\[\]\\';,/{}|\\":<>\?]/g.test(value)) ? '英数字のみ' : undefined;

const phoneNumber = (value) => value && !/^(0|[1-9][0-9]{9})$/i.test(value) ? 'Invalid phone number, must be 10 digits' : undefined;

const validateEmail = (value) => (typeof email(value) === 'undefined');

const validateAlphaNumeric = (value) => (typeof alphaNumeric(value) === 'undefined');

const validateNumber = (value) => (typeof number(value) === 'undefined');

const minCharacter = (value, min) => value && value.length < min ? `${min}文字以上 ` : undefined;

// Validate WhiteSpace
const validateWhiteSpace = (value) => {
  if (typeof value === 'undefined') {
    return '※未入力です。';
  }
  if (value.trim().length === 0) {
    return 'white space';
  }
  return undefined;
};

export {
  required, maxLength, minLength, number, minValue, email, tooOld, aol, alphaNumeric, phoneNumber, minCharacter,
  validateNumber, validateAlphaNumeric, validateEmail, validateWhiteSpace,
};
